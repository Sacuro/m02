#  2 - Sistemes Operatius Linux 1

Taula de continguts

[TOC]

## 2.1 - Conceptes bàsics

### 2.1.1 - Comandes

Comandos Básicos de Linux

- **man**: ajuda i documentació
- **ls**: Llistar arxius i carpetes
- **cd**: Canviar de directori
- **mkdir**: Crear nou directori
- **touch**: Crear un nou arxiu
- **rm**: Esborrar arxius i directoris
- **cp**: Copiar arxius i directoris
- **mv**: Moure arxius i directoris
- **cat/tail/head**: Veure contingut arxius
- **vi/vim/nano**: Editar contingut arxius
- **su**: Canviar al mode superusuari o d'usuari
- **sudo**: Executar en mode superusuari
- **passwd**: Canviar clau usuari
- **tar/zip/unzip/rar/unrar**: Comprimir i descomprimir fitxers.
- **reboot**: Reiniciar sistema operatiu
- **halt**: Apagar el sistema operatiu
- **clear**: Netejar terminal
- **exit**: Sortir del terminal
        
### 2.1.2 - Permisos

```markdown
# PERMISOS D'ARXIUS I DIRECTORIS
Podem veure els permisos i propietaris d'un arxiu o directori amb la comanda ls -lisa.

jvinolas@samba ~$ ls -lisa
total 162628
16778303      4 drwx------ 34 jvinolas inf                 4096 11 jul 10:47 .
15728652      4 drwxr-x--x 37 jordinas Administrators      4096 26 abr 10:34 ..
16778304     16 -rw-------  1 jvinolas inf                13331  9 jul 08:57 .bash_history
16908864      4 drwx------ 14 jvinolas inf                 4096 26 abr 11:22 .cache
16778431      4 drwxr-xr-x 30 jvinolas inf                 4096 11 gen 16:28 .config
16779156      4 -rw-r--r--  1 jvinolas inf                 1788 20 gen  2016 consola.vv
16912376      4 drwx------  3 jvinolas inf                 4096 12 feb  2014 .dbus
16779163      4 -rw-r-----  1 jvinolas inf                 1079  5 set  2014 debug.log
16778413      4 drwxr-xr-x  2 jvinolas inf                 4096  4 mai  2017 Descargas
16908712      4 drwxr-xr-x  2 jvinolas inf                 4096  4 mai  2017 Documentos
16908566      4 drwx------  2 jvinolas inf                 4096  6 mai  2014 Downloads

Si us fixeu en la tercera columna hi ha l'identificador de si es tracta o no d'un directori amb la lletra d.
A continuació en aquesta tercera columna hi ha repetits els permisos del fitxer tres vegades:
- r: read
- w: write
- x: execute
I fan referència als permisos de:
1) usuari
2) grup
3) altres
De manera que els permisos: -rw-r----- ens indiquen que l'usuari té permisos de lectura i escriptura sobre aquest fitxer, el grup només de lectura i la resta d'usuaris i grups no tenen cap permís sobre aquest fitxer.
L'usuari al que fa referència es tra a la quarta columna: jvinolas. I el grup al costat: inf
Per a modificar els permisos podem utilitzar la comanda chmod.
Per a modificar els propietaris podem utilitzar la comanda chown.
Feu un man chown i un man chmod per veure com fer-ne ús i proveu de canviar-ne usuaris, grups i permisos.
Per afegir usuaris al sistema ho farem amb adduser i per a grups groupadd.
```

### 2.1.3 - Rutes absolutes i relatives

En especificar els arxius en el directori de treball actual, es pot fer referència a ells només amb el nom d'arxiu corresponent. Però, en fer referència a directoris i arxius que estiguin fora del directori de treball actual, hem d'utilitzar noms amb
rutes, les quals li indiquen a Linux com arribar al directori apropiat.

#### Rutes absolutes
Els noms amb rutes absolutes especifiquen la ruta que condueix a un directori o arxiu, començant pel directori arrel en la part superior de l'estructura d'arbre invertit. El directori arrel es representa amb una barra (/). La ruta consisteix en una llista seqüencial de directoris, separats per barres, que condueixen al directori o a l'arxiu que desitgi especificar. L'últim nom de la ruta és el directori o l'arxiu al qual es desitja arribar.

A continuació, es presenta un exemple d'una ruta absoluta per mostrar documents:

```
$ pwd
/home/darta
$ ls /home/darta/documents
```

L'anterior especifica la ubicació del directori actual, luisa, començant pel directori arrel i seguint una trajectòria descendent.
La següent figura mostra els noms de rutes absolutes per a diversos directoris i arxius d'una estructura de directoris típica:

#### Rutes relatives
Pots utilitzar un nom de ruta relativa com a accés directe a la ubicació dels arxius i directoris. Els noms de rutes relatives especifiquen directoris i arxius començant pel directori de treball actual (en lloc del directori arrel).

```
$ pwd
/home/darta
$ ls documents
```

Per a obtenir una llista dels arxius d'un directori que està immediatament sota el directori actual, n'hi ha prou amb escriure el nom del directori. Per exemple, per a obtenir una llista dels arxius del directori projectes, que està per sota del directori actual:

```
$ pwd
/home/darta
$ ls ..
```
Això mostrarà el contingut de la carpeta immediatament superior (/home)


### 2.1.4 - Enllaços simbòlics

Diferencies que existeixen en Unix / Linux entre els enllaços simbòlics (soft o symbolic links) i els enllaços durs (hard links).

#### Enllaços simbòlics (soft / symbolic links)

La manera més senzilla de comprendre que és un enllaç simbòlic en Linux és comparar-lo amb el “enllaç directe” o “shortcut” en Windows. El fitxer o directori es troba en un únic punt del disc i els enllaços són un punter contra ell. Cada enllaç simbòlic té el seu propi número de inodo el que permet fer enllaços simbòlics entre diferents sistemes de fitxers.

Per a crear enllaços (tant simbòlics com duros) usem el comando ln. En aquest cas crearem un enllaç simbòlic (paràmetre -s) del fitxer test:

```
$ ln -s test enllaci-a-test
```

Si llistem tots dos veurem que l'enllaç té el caràcter l que l'identifica com a enllaç simbòlic:

```
$ ls -l
lrwxrwxrwx 1 alex alex 4 2011-04-27 18.59 enllaç-a-test -> test
-rw-r--r-- 1 alex alex 0 2011-04-27 18.58 test
```

Per a confirmar que l'enllaç simbòlic té un inodo diferent usem el comando stat:

```
$ stat test
File: «test»
Size: 0 Blocks: 0 IO Block: 4096 arxiu regular buit
Device: 804h/2052d Inode: 73793 Links: 1
Access: (0644/-rw-r--r--) Uid: ( 1000/ alex) Gid: ( 1000/ alex)
Access: 2011-04-27 18.58:53.124142406 +0200
Modify: 2011-04-27 18.58:53.124142406 +0200
Change: 2011-04-27 18.58:53.124142406 +0200

$ stat enllaç-a-test
File: «enllaç-a-test» -> «test»
Size: 4 Blocks: 0 IO Block: 4096 vincle simbòlic
Device: 804h/2052d Inode: 77212 Links: 1
Access: (0777/lrwxrwxrwx) Uid: ( 1000/ alex) Gid: ( 1000/ alex)
Access: 2011-04-27 18.59:07.812139890 +0200
Modify: 2011-04-27 18.59:06.460112888 +0200
Change: 2011-04-27 18.59:06.460112888 +0200
```

També ho podem verificar traient el inodo en el ls (-i):

```
$ ls -li
77212 lrwxrwxrwx 1 alex alex 4 2011-04-27 18.59 enllaç-a-test -> test
73793 -rw-r--r-- 1 alex alex 0 2011-04-27 18.58 test
```

Cal tenir en compte, que en Linux / Unix (igual que amb els accessos directes de Windows), si esborrem el fitxer o directori origen, l'enllaç simbòlic roman però les dades desapareixen per sempre.

#### Enllaços durs (hard links)

Els enllaços durs el que fan és associar dos o més fitxers compartint el mateix inodo. Això fa que cada enllaç dur és una còpia exacta de la resta de fitxers associats, tant de dades com de permisos, propietari, etc. Això implica també que quan es realitzin canvis en un dels enllaços o en el fitxer aquest també es realitzarà en la resta d'enllaços.

Els enllaços durs no poden fer-se contra directoris i tampoc fora del propi sistema de fitxers.

Crearem un hard link contra el fitxer “test” d'abans i veurem que efectivament comparteixen inodo i que les dades se sincronitzen entre tots dos:

```
$ ln test enllaç-dur-test
$ ls -li
73793 -rw-r--r-- 2 alex alex 5 2011-04-27 19.09 enllaç-dur-test
73793 -rw-r--r-- 2 alex alex 5 2011-04-27 19.09 test
```

En la primera columna verifiquem que tenen el mateix número de inodo i en la tercera s'especifica quan enllaços durs té el fitxer. Si feu canvis en un d'ells veureu que també es fan en la resta. Si per exemple canviem els permisos al fitxer test:

```
$ chmod 0755 test
$ ls -li
73793 -rwxr-*xr-x 2 alex alex 5 2011-04-27 19.09 enllaç-dur-test
73793 -rwxr-*xr-x 2 alex alex 5 2011-04-27 19.09 test
```

I finalment el stat de cadascun verifica tot el que comentem:

```
$ stat test
File: «test»
Size: 5 Blocks: 8 IO Block: 4096 arxiu regular
Device: 804h/2052d Inode: 73793 Links: 2
Access: (0755/-rwxr-*xr-x) Uid: ( 1000/ alex) Gid: ( 1000/ alex)
Access: 2011-04-27 19.09:51.528132995 +0200
Modify: 2011-04-27 19.09:53.640114896 +0200
Change: 2011-04-27 19.11:42.516138726 +0200

$ stat enllaç-dur-test
File: «enllaç-dur-test»
Size: 5 Blocks: 8 IO Block: 4096 arxiu regular
Device: 804h/2052d Inode: 73793 Links: 2
Access: (0755/-rwxr-*xr-x) Uid: ( 1000/ alex) Gid: ( 1000/ alex)
Access: 2011-04-27 19.09:51.528132995 +0200
Modify: 2011-04-27 19.09:53.640114896 +0200
Change: 2011-04-27 19.11:42.516138726 +0200
```

#### Diferències entre soft i hard links

- Els enllaços simbòlics es poden fer amb fitxers i directoris mentre que els duros sol entre fitxers.
- Els enllaços simbòlics es poden fer entre diferents sistemes de fitxers, els duros no.
- Els enllaços durs comparteixen el número de inodo, els simbòlics no.
- En els enllaços simbòlics si s'esborra el fitxer o directori original, la informació es perd, en els duros no.
- Els enllaços durs són còpies exactes del fitxer mentre que els simbòlics són mers punters o “accessos directes”.

### 2.1.5 Comodins

Existeixen en Linux un conjunt de caràcters la combinació dels quals permet que l'intèrpret d'ordres els substitueixi per un grup de símbols. Aquests caràcters es denominen caràcters comodí. De els existents destaquem els següents:
* Representa qualsevol conjunt de símbols. Per exemple, sabent que l'ordre ls permet mostrar els arxius presents en un directori:
```
[jordi@oracle jordi]$ls *   # mostra tots els arxius del directori de treball
[jordi@oracle jordi]$ls ab* # mostra els arxius que comencin per ab
```
? Representa qualsevol caràcter.
```
[jordi@oracle jordi]$ls a?b # mostra els arxius els noms dels quals tinguin tres caràcters, el primer sigui una a i l'últim una b
[...] Representen un símbol del conjunt.
[jordi@oracle jordi]$ls ab[123] # mostra (si existeixen) els arxius ab1, ab2 i ab3
```
[!...] Representa un símbol NO contingut en el conjunt.

```
[jordi@oracle jordi]$ls [!ab]* # mostra tots els arxius el nom dels quals NO comença per ni per a ni per
```
De manera general per a tots els caràcters especials i comodí, s'habiliten diferents maneres per a forçar al fet que la shell els interpreti literalment com els caràcters que són. Les maneres d'escapar els caràcters especials i comodí són els següents:
Utilitzant el caràcter \ es poden escapar caràcters individualment.
Quan es vol escapar cadenes completes es recorre a tancar-los entre cometes '.
[jordi@oracle jordi]$ls 'test?&' # mostra l'arxiu amb nom test?&
[jordi@oracle jordi]$ls \*a      # mostra l'arxiu de nom a


### 2.1.6 Búsquedes

#### Grep

Grep és una ordre molt útil que permet buscar text (una o diverses paraules) dins d'un o més arxius. Per exemple, busquem la paraula "text" dins de tots els arxius de text (.txt) que tinguem en el directori "home":

```
grep "text" /home/ *.txt
```
Si volem buscar en aquest directori i en els quals estan dins d'ell:
```
grep -r "text" /home/ *.txt
```
Buscar en aquest directori, en els quals estan dins d'ell i en qualsevol tipus d'arxiu:
```
grep -r "text" /home/
```
Buscar la paraula "text" dins d'un arxiu ignorant distinció entre majúscules i minúscules:
```
grep -i "text" nomeni-del-arxiu
```
Buscar comentaris dins d'un arxiu (els comentaris habitualment comencen amb un comodí):
```
grep ^# nom-del-arxiu
```

### 2.1.7 Operadors

b A part dels caràcters comodí, existeixen altres caràcters el significat dels quals és especial per a la shell. Entre aquests destaquem:
```
< , > , | , &
```

#### Operador &

Posat al final d'una ordre l'executa en segon plà (background):
```
ping www.linux.org &
```
Recordeu que podeu recuperar una ordre de segon plà amb **fg**

#### Operador ;

Ens permet executar consecutivament diverses comandes si el posem entre elles:

```
ls .; ls /;
```

#### Operador &&

És com l'operador ; però només executarà les següents ordres si les anteriors no donen com a resultat un error.

Les ordres de linux donen un 0 com a estat si s'ha executat correctament i un altre número si ha donat error. Per comprovar el resultat d'una comanda després d'executar-la ho podeu fer amb **echo $?** immediatament després.

Per exemple, comproveu el resultat de l'estat en executar:
```
ls
echo $?
ls /aksdjhfwoife
echo $?
```
Podeu comprovar que el segon ls ha donat error. Això farà que el resultat d'executar la seqüència d'ordres següent sigui diferent en cada cas:
```
ls && ls /tmp
ls /ñaolsdkfjñasld && ls /tmp
```

#### Operador ||

En aquest cas, en concatenar ordres, la segona només s'executarà si la primera ha donat error.
```
ls || ls /tmp
ls /ñaolsdkfjñasld || ls /tmp
```

#### Operador !

Ens permet la negació, fer el contrari. Per exemple aquesta comanda eliminaria tots els fitxers amb extensió html recursivament:
```
rm -r *.html
```
En canvi si volem eliminar tot excepte els que tenen extensió html farem la negació:
```
rm -r !(*.html)
```

#### Pipe |

Una 'pipe' ens permet enviar el resultat de la primera ordre com a entrada de la segona. Per exemple:
```
ls -lh | less
```
Farà que en comptes de sortir per pantalla el resultat de **ls -lh** (que seria el que passaria per defecte) s'envia com a entrada de la comanda less.

#### Redirecció a fitxers >, >>

Amb aquests redirectors podem enviar la sortida de la comanda a un fitxer:
```
ls > fitxer.out
ls >> fitxer.out
```
Mentre que l'operador **>** sobreescriurà sempre el que hi hagi al fitxer destí, l'operador **>>** ho afegeix al final del fitxer (si ja existeix).

### 2.1.8 Ordres per processar fitxers

#### cut

La comanda cut ens permet extreure camps, per defecte separats per tabuladors, d'un llistat. Per exemple si tenim en un fitxer:

```
one	two	three	four	five
alpha	beta	gamma	delta	epsilon
```

Amb la comanda ```cut -f 3 data.txt``` obtindrem:

```
three
gamma
```

que són els camps en tercera posició

#### sort

Ens permetrà ordenar-los. Per exemple, si tenim:

```
apples
oranges
pears
kiwis
bananas
```

I fem:

```
sort data.txt
```

Obtindrem:

```
apples
bananas
kiwis
oranges
pears
```

#### uniq

Ens permet filtrar línies repetides. Per exemple si tenim:

```
This is a line.
This is a line.
This is a line.
 
This is also a line.
This is also a line.
 
This is also also a line.
```

I executem **uniq data.txt** obtindrem:

```
This is a line.
 
This is also a line.
 
This is also also a line.
```

Utilitzeu sempre el manual per buscar les opcions i paràmetres de cada ordre.

### 2.1.9 Variables d'entorn

Font: http://linuxemb.wikidot.com/variables-entorno

Una variable d'entorn és un nom associat a una cadena de caràcters.

Depenent  de la variable, la seva utilitat pot ser diferent. Algunes són útils  per a no haver d'escriure moltes opcions en executar un programa, unes  altres les utilitza el propi shell (PATH, PS1,…). La taula següent mostra la llista de variables més usuals:

- DISPLAY: On apareixen la sortides de X-Windows.
- HOME: Directori personal.
- HOSTNAME: Nom de la màquina.
- MAIL: Arxiu de correu.
- PATH: Llista de directoris on buscar els programes.
- PS1: Prompt.
- SHELL: Intèrpret de comandos per defecte.
- TERM: Tipus de terminal.
- USER: Nom de l'usuari.

La forma de definir una variable d'entorn canvia amb l'interpret d'ordres, es mostra tcsh i bash sent els dos mes populars en l'àmbit Linux:

- bash: ```export VARIABLE=Valor```
- tcsh: ```setenv VARIABLE Valor```

Per exemple, per a definir el valor de la variable DISPLAY:

- bash: ```export DISPLAY=localhost:0.0```
- tcsh: ```setenv DISPLAY localhost:0.0```

La variable PATH s'exporta de la següent manera:

- export PATH=……:$PATH

Per a veure el contingut de les variables d'entorn

- env

i si volem veure d'una en particular:

- echo $PATH

## 2.2 Scripts

### 2.2.1 Encapçalament

Un script sempre haurà de contenir una capçalera on s'indicaran les dades següents comentades amb un **#**:

```bash
#!/bin/bash
# Filename:       script.sh
# Author:         Cognoms, Nom  (codi)
# Date:           dia/mes/any
# Version:        0.1
# License:        This is free software, licensed under the GNU General Public License v3.
#                 See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:          ./script.sh
# Description:    Expliqueu el que farà l'script
```

A partir d'aquí posarem les ordres i, si cal, també comentaris iniciant la línia amb un **#**.

#### Shebang

Shebang és, en l'argot d'Unix, el nom que rep el parell de caràcters "#!", que es troben al principi d'un fitxer de tipus script. En sistemes operatius de tipus Unix, la presència d'aquests dos caràcters indica que el fitxer és un script o fitxer de comandes, i que cal executar-lo utitlitzant l'intèrpret especificat a continuació, a la resta de la primera línia del fitxer.

Més informació a [wikipedia](https://en.wikipedia.org/wiki/Shebang_%28Unix%29)

### 2.2.2 Exemples

En realitat fer un script pot ser simplement posar una sèrie d'ordres en un fitxer executable (ometem aquí l'encapçalament per simplificar):

```bash
#!/bin/bash
echo "Hello World"
```

Usarem habitualment variables

```bash
#!/bin/bash
# Add two numeric value
((sum=25+35))

#Print the result
echo $sum
```

Podem demanar a l'usuari que ens escrigui coses que posarem en una variable

```bash
#!/bin/bash
echo "Enter Your Name"
read name
echo "Welcome $name to LinuxHint"
```

A un script també podrem passar-li arguments de manera que dins de l'script hi podrem accedir mitjançant  les variables $1, $2... Per exemple si aquest script s'anomena *compta.sh*  i el cridem amb ```compta.sh fitxer.txt``` el valor de la variable FILE1 serà *fitxer.txt+*:

```bash
#!/bin/bash
FILE1=$1
wc $FILE1
```

També podrem permetre un nombre variable d'arguments i accedir-hi dins l'script amb la variable **$@** que és una llista (array) amb tots els arguments que s'han passar a l'script:

```bash
#!/bin/bash
for FILE1 in "$@"
do
wc $FILE1
done
```

En l'anterior script hi tenim un **bucle for** (for loop). Explicat vindria a ser més o menys:

```bash
#!/bin/bash
per cada FILE1 a "$@"
fes
wc $FILE1
acaba
```

Un script més elaborat farà servir el mètode de **banderes** (flags) que permet indicar a què correspon cada argument. Per exemple podem cridar un script amb ```makereport.sh -u jsmith -p notebooks -d 10-20-2011 -f pdf``` i dins processar cada argument per separat segons a quin *flag* correspon:

```bash
#!/bin/bash
while getopts u:d:p:f: option
do
case "${option}"
in
u) USER=${OPTARG};;
d) DATE=${OPTARG};;
p) PRODUCT=${OPTARG};;
f) FORMAT=$OPTARG;;
esac
done
```

En l'anterior script tenim un **bucle while** (while loop). Que explicat vindria a ser:

```bash
#!/bin/bash
mentres getopts u:d:p:f: option
fes
segons el cas "${option}"
sigui
u) USER=${OPTARG};;
d) DATE=${OPTARG};;
p) PRODUCT=${OPTARG};;
f) FORMAT=$OPTARG;;
fi de segons el cas
acaba
```





